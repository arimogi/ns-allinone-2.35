/*              Copyright (C) 2010
	*             Multipath Extension by Jiazi Yi,                            *
 	*                   2007   Ecole Polytech of Nantes, France               * 
 	*                   jiazi.yi@univ-nantes.fr				   *
 	****************************************************************************
 	*    	This program is distributed in the hope that it will be useful,				*
	*    	but WITHOUT ANY WARRANTY; without even the implied warranty of				*
	*    	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 					*
 	**************************************************************************
*/
/***************************************************************************
 *   Copyright (C) 2004 by Francisco J. Ros                                *
 *   fjrm@dif.um.es                                                        *
 *                                                                         *
 *             Multipath Extension by Jiazi Yi,                            *
 *                   2007   Ecole Polytech of Nantes, France               * 
 *                   jiazi.yi@univ-nantes.fr				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

///
/// \file	KMPOLSR.h
/// \brief	Header file for KMPOLSR agent and related classes.
///
/// Here are defined all timers used by KMPOLSR, including those for managing internal
/// state and those for sending messages. Class KMPOLSR is also defined, therefore this
/// file has signatures for the most important methods. Lots of constants are also
/// defined.
///

#ifndef __KMPOLSR_h__
#define __KMPOLSR_h__

#include "KMPOLSR_pkt.h"
#include "KMPOLSR_state.h"
#include "KMPOLSR_rtable.h"
#include "KMPOLSR_m_rtable.h"
#include "KMPOLSR_repositories.h"
#include "trace.h"
#include "classifier-port.h"
#include "agent.h"
#include "packet.h"
#include "timer-handler.h"
#include "random.h"
#include "vector"
#include <dsr-priqueue.h>

//-- Reza Andria
#include "mobilenode.h" 

/********** Useful macros **********/

/// Returns maximum of two numbers.
#ifndef MAX
#define	MAX(a, b) (((a) > (b)) ? (a) : (b))
#endif

/// Returns minimum of two numbers.
#ifndef MIN
#define	MIN(a, b) (((a) < (b)) ? (a) : (b))
#endif

/// Defines NULL as zero, used for pointers.
#ifndef NULL
#define NULL 0
#endif

/// Gets current time from the scheduler.
#define CURRENT_TIME	Scheduler::instance().clock()

///
/// \brief Gets the delay between a given time and the current time.
///
/// If given time is previous to the current one, then this macro returns
/// a number close to 0. This is used for scheduling events at a certain moment.
///
#define DELAY(time) (((time) < (CURRENT_TIME)) ? (0.000001) : \
	(time - CURRENT_TIME + 0.000001))

/// Scaling factor used in RFC 3626.
#define KMPOLSR_C		0.0625


/********** Intervals **********/

/// HELLO messages emission interval.
#define KMPOLSR_HELLO_INTERVAL	 2

/// TC messages emission interval.
#define KMPOLSR_TC_INTERVAL	 5

/// MID messages emission interval.
#define KMPOLSR_MID_INTERVAL	KMPOLSR_TC_INTERVAL

///
/// \brief Period at which a node must cite every link and every neighbor.
///
/// We only use this value in order to define KMPOLSR_NEIGHB_HOLD_TIME.
///
#define KMPOLSR_REFRESH_INTERVAL	2


/********** Holding times **********/

/// Neighbor holding time.
#define KMPOLSR_NEIGHB_HOLD_TIME	3*KMPOLSR_REFRESH_INTERVAL
/// Top holding time.
#define KMPOLSR_TOP_HOLD_TIME	3*KMPOLSR_TC_INTERVAL
/// Dup holding time.
#define KMPOLSR_DUP_HOLD_TIME	30
/// MID holding time.
#define KMPOLSR_MID_HOLD_TIME	3*KMPOLSR_MID_INTERVAL


/********** Link types **********/

/// Unspecified link type.
#define KMPOLSR_UNSPEC_LINK	0
/// Asymmetric link type.
#define KMPOLSR_ASYM_LINK		1
/// Symmetric link type.
#define KMPOLSR_SYM_LINK		2
/// Lost link type.
#define KMPOLSR_LOST_LINK		3

/********** Neighbor types **********/

/// Not neighbor type.
#define KMPOLSR_NOT_NEIGH		0
/// Symmetric neighbor type.
#define KMPOLSR_SYM_NEIGH		1
/// Asymmetric neighbor type.
#define KMPOLSR_MPR_NEIGH		2


/********** Willingness **********/

/// Willingness for forwarding packets from other nodes: never.
#define KMPOLSR_WILL_NEVER		0
/// Willingness for forwarding packets from other nodes: low.
#define KMPOLSR_WILL_LOW		1
/// Willingness for forwarding packets from other nodes: medium.
#define KMPOLSR_WILL_DEFAULT	3
/// Willingness for forwarding packets from other nodes: high.
#define KMPOLSR_WILL_HIGH		6
/// Willingness for forwarding packets from other nodes: always.
#define KMPOLSR_WILL_ALWAYS	7


/********** Miscellaneous constants **********/

/// Maximum allowed jitter.
#define KMPOLSR_MAXJITTER		KMPOLSR_HELLO_INTERVAL/4
/// Maximum allowed sequence number.
#define KMPOLSR_MAX_SEQ_NUM	65535
/// Used to set status of an KMPOLSR_nb_tuple as "not symmetric".
#define KMPOLSR_STATUS_NOT_SYM	0
/// Used to set status of an KMPOLSR_nb_tuple as "symmetric".
#define KMPOLSR_STATUS_SYM		1
/// Random number between [0-KMPOLSR_MAXJITTER] used to jitter KMPOLSR packet transmission.
#define JITTER			(Random::uniform()*KMPOLSR_MAXJITTER)

/*****************Type of Dijsktra node***********/
#define P_TYPE 1
#define T_TYPE 0

#define MAX_ROUTE 3

#define USE_MAC TRUE

class KMPOLSR;			// forward declaration


/********** Timers **********/


/// Timer for sending an enqued message.
class KMPOLSR_MsgTimer : public TimerHandler {
public:
	KMPOLSR_MsgTimer(KMPOLSR* agent) : TimerHandler() {
		agent_	= agent;
	}
protected:
	KMPOLSR*	agent_;			///< KMPOLSR agent which created the timer.
	virtual void expire(Event* e);
};

/// Timer for sending HELLO messages.
class KMPOLSR_HelloTimer : public TimerHandler {
public:
	KMPOLSR_HelloTimer(KMPOLSR* agent) : TimerHandler() { agent_ = agent; }
protected:
	KMPOLSR*	agent_;			///< KMPOLSR agent which created the timer.
	virtual void expire(Event* e);
};


/// Timer for sending TC messages.
class KMPOLSR_TcTimer : public TimerHandler {
public:
	KMPOLSR_TcTimer(KMPOLSR* agent) : TimerHandler() { agent_ = agent; }
protected:
	KMPOLSR*	agent_;			///< KMPOLSR agent which created the timer.
	virtual void expire(Event* e);
};


/// Timer for sending MID messages.
class KMPOLSR_MidTimer : public TimerHandler {
public:
	KMPOLSR_MidTimer(KMPOLSR* agent) : TimerHandler() { agent_ = agent; }
protected:
	KMPOLSR*	agent_;			///< KMPOLSR agent which created the timer.
	virtual void expire(Event* e);
};


/// Timer for removing duplicate tuples: KMPOLSR_dup_tuple.
class KMPOLSR_DupTupleTimer : public TimerHandler {
public:
	KMPOLSR_DupTupleTimer(KMPOLSR* agent, KMPOLSR_dup_tuple* tuple) : TimerHandler() {
		agent_ = agent;
		tuple_ = tuple;
	}
protected:
	KMPOLSR*		agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_dup_tuple*	tuple_;	///< KMPOLSR_dup_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/// Timer for removing link tuples: KMPOLSR_link_tuple.
class KMPOLSR_LinkTupleTimer : public TimerHandler {
	///
	/// \brief A flag which tells if the timer has expired (at least) once or not.
	///
	/// When a link tuple has been just created, its sym_time is expired but this
	/// does not mean a neighbor loss. Thus, we use this flag in order to be able
	/// to distinguish this situation.
	///
	bool			first_time_;
public:
	KMPOLSR_LinkTupleTimer(KMPOLSR* agent, KMPOLSR_link_tuple* tuple) : TimerHandler() {
		agent_		= agent;
		tuple_		= tuple;
		first_time_	= true;
	}
protected:
	KMPOLSR*			agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_link_tuple*	tuple_;	///< KMPOLSR_link_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/// Timer for removing nb2hop tuples: KMPOLSR_nb2hop_tuple.
class KMPOLSR_Nb2hopTupleTimer : public TimerHandler {
public:
	KMPOLSR_Nb2hopTupleTimer(KMPOLSR* agent, KMPOLSR_nb2hop_tuple* tuple) : TimerHandler() {
		agent_ = agent;
		tuple_ = tuple;
	}
protected:
	KMPOLSR*			agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_nb2hop_tuple*	tuple_;	///< KMPOLSR_nb2hop_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/// Timer for removing MPR selector tuples: KMPOLSR_mprsel_tuple.
class KMPOLSR_MprSelTupleTimer : public TimerHandler {
public:
	KMPOLSR_MprSelTupleTimer(KMPOLSR* agent, KMPOLSR_mprsel_tuple* tuple) : TimerHandler() {
		agent_ = agent;
		tuple_ = tuple;
	}
protected:
	KMPOLSR*			agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_mprsel_tuple*	tuple_;	///< KMPOLSR_mprsel_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/// Timer for removing topology tuples: KMPOLSR_topology_tuple.
class KMPOLSR_TopologyTupleTimer : public TimerHandler {
public:
	KMPOLSR_TopologyTupleTimer(KMPOLSR* agent, KMPOLSR_topology_tuple* tuple) : TimerHandler() {
		agent_ = agent;
		tuple_ = tuple;
	}
protected:
	KMPOLSR*			agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_topology_tuple*	tuple_;	///< KMPOLSR_topology_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/// Timer for removing interface association tuples: KMPOLSR_iface_assoc_tuple.
class KMPOLSR_IfaceAssocTupleTimer : public TimerHandler {
public:
	KMPOLSR_IfaceAssocTupleTimer(KMPOLSR* agent, KMPOLSR_iface_assoc_tuple* tuple) : TimerHandler() {
		agent_ = agent;
		tuple_ = tuple;
	}
protected:
	KMPOLSR*			agent_;	///< KMPOLSR agent which created the timer.
	KMPOLSR_iface_assoc_tuple*	tuple_;	///< KMPOLSR_iface_assoc_tuple which must be removed.
	
	virtual void expire(Event* e);
};


/********** KMPOLSR Agent **********/


///
/// \brief Routing agent which implements %KMPOLSR protocol following RFC 3626.
///
/// Interacts with TCL interface through command() method. It implements all
/// functionalities related to sending and receiving packets and managing
/// internal state.
///
class KMPOLSR : public Agent {
	// Makes some friends.
	friend class KMPOLSR_HelloTimer;
	friend class KMPOLSR_TcTimer;
	friend class KMPOLSR_MidTimer;
	friend class KMPOLSR_DupTupleTimer;
	friend class KMPOLSR_LinkTupleTimer;
	friend class KMPOLSR_Nb2hopTupleTimer;
	friend class KMPOLSR_MprSelTupleTimer;
	friend class KMPOLSR_TopologyTupleTimer;
	friend class KMPOLSR_IfaceAssocTupleTimer;
	friend class KMPOLSR_MsgTimer;
	
	/// Address of the routing agent.
	nsaddr_t	ra_addr_;
	
	/// Packets sequence number counter.
	u_int16_t	pkt_seq_;
	/// Messages sequence number counter.
	u_int16_t	msg_seq_;
	/// Advertised Neighbor Set sequence number.
	u_int16_t	ansn_;
	
	/// HELLO messages' emission interval.
	int		hello_ival_;
	/// TC messages' emission interval.
	int		tc_ival_;
	/// MID messages' emission interval.
	int		mid_ival_;
	/// Willingness for forwarding packets on behalf of other nodes.
	int		willingness_;
	/// Determines if layer 2 notifications are enabled or not.
	int		use_mac_;
	
	/// Routing table.
	KMPOLSR_rtable		rtable_;

	/// Multipath routing table
	KMPOLSR_m_rtable		m_rtable_;

	
	/// Internal state with all needed data structs.
	KMPOLSR_state		state_;
	/// A list of pending messages which are buffered awaiting for being sent.
	std::vector<KMPOLSR_msg>	msgs_;

	///packet count
	int 		packet_count_;

	//-- untuk menyimpan nodalDegree
	MobileNode *myNode;
	
protected:
	PortClassifier*	dmux_;		///< For passing packets up to agents.
	Trace*		logtarget_;	///< For logging.
	
	KMPOLSR_HelloTimer	hello_timer_;	///< Timer for sending HELLO messages.
	KMPOLSR_TcTimer	tc_timer_;	///< Timer for sending TC messages.
	KMPOLSR_MidTimer	mid_timer_;	///< Timer for sending MID messages.
	
	/// Increments packet sequence number and returns the new value.
	inline u_int16_t	pkt_seq() {
		pkt_seq_ = (pkt_seq_ + 1)%(KMPOLSR_MAX_SEQ_NUM + 1);
		return pkt_seq_;
	}
	/// Increments message sequence number and returns the new value.
	inline u_int16_t	msg_seq() {
		msg_seq_ = (msg_seq_ + 1)%(KMPOLSR_MAX_SEQ_NUM + 1);
		return msg_seq_;
	}
	
	inline nsaddr_t&	ra_addr()	{ return ra_addr_; }
	
	inline int&		hello_ival()	{ return hello_ival_; }
	inline int&		tc_ival()	{ return tc_ival_; }
	inline int&		mid_ival()	{ return mid_ival_; }
	inline int&		willingness()	{ return willingness_; }
	inline int&		use_mac()	{ return use_mac_; }
	
	inline linkset_t&	linkset()	{ return state_.linkset(); }
	inline mprset_t&	mprset()	{ return state_.mprset(); }
	inline mprselset_t&	mprselset()	{ return state_.mprselset(); }
	inline nbset_t&		nbset()		{ return state_.nbset(); }
	inline nb2hopset_t&	nb2hopset()	{ return state_.nb2hopset(); }
	inline topologyset_t&	topologyset()	{ return state_.topologyset(); }
	inline dupset_t&	dupset()	{ return state_.dupset(); }
	inline ifaceassocset_t&	ifaceassocset()	{ return state_.ifaceassocset(); }
	
	void		recv_kmpolsr(Packet*);
	
	void		mpr_computation();
	void		rtable_computation();
	void 		m_rtable_computation(Packet*);

	void		process_hello(KMPOLSR_msg&, nsaddr_t, nsaddr_t);
	void		process_tc(KMPOLSR_msg&, nsaddr_t);
	void		process_mid(KMPOLSR_msg&, nsaddr_t);
	
	void		forward_default(Packet*, KMPOLSR_msg&, KMPOLSR_dup_tuple*, nsaddr_t);
	void		forward_data(Packet*);
	void 		m_forward_data(Packet*);
	void		resend_data(Packet*);
	
	void		enque_msg(KMPOLSR_msg&, double);
	void		send_hello();
	void		send_tc();
	void		send_mid();
	void		send_pkt();
	
	void		link_sensing(KMPOLSR_msg&, nsaddr_t, nsaddr_t);
	void		populate_nbset(KMPOLSR_msg&);
	void		populate_nb2hopset(KMPOLSR_msg&);
	void		populate_mprselset(KMPOLSR_msg&);

	void		set_hello_timer();
	void		set_tc_timer();
	void		set_mid_timer();

	void		nb_loss(KMPOLSR_link_tuple*);
	void		add_dup_tuple(KMPOLSR_dup_tuple*);
	void		rm_dup_tuple(KMPOLSR_dup_tuple*);
	void		add_link_tuple(KMPOLSR_link_tuple*, u_int8_t);
	void		rm_link_tuple(KMPOLSR_link_tuple*);
	void		updated_link_tuple(KMPOLSR_link_tuple*);
	void		add_nb_tuple(KMPOLSR_nb_tuple*);
	void		rm_nb_tuple(KMPOLSR_nb_tuple*);
	void		add_nb2hop_tuple(KMPOLSR_nb2hop_tuple*);
	void		rm_nb2hop_tuple(KMPOLSR_nb2hop_tuple*);
	void		add_mprsel_tuple(KMPOLSR_mprsel_tuple*);
	void		rm_mprsel_tuple(KMPOLSR_mprsel_tuple*);
	void		add_topology_tuple(KMPOLSR_topology_tuple*);
	void		rm_topology_tuple(KMPOLSR_topology_tuple*);
	void		add_ifaceassoc_tuple(KMPOLSR_iface_assoc_tuple*);
	void		rm_ifaceassoc_tuple(KMPOLSR_iface_assoc_tuple*);
	
	nsaddr_t	get_main_addr(nsaddr_t);
	int		degree(KMPOLSR_nb_tuple*);

	static bool	seq_num_bigger_than(u_int16_t, u_int16_t);
	float 		tuple_weight(float a);

	NsObject *ll;		        // our link layer output 
	CMUPriQueue *ifq;		// output interface queue
	Mac *mac_;

public:
	KMPOLSR(nsaddr_t);
	int	command(int, const char*const*);
	void	recv(Packet*, Handler*);
	void	mac_failed(Packet*);
	
	static double		emf_to_seconds(u_int8_t);
	static u_int8_t		seconds_to_emf(double);
	static int		node_id(nsaddr_t);
};

#endif
