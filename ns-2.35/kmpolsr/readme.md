KMPOLSR: Kinetic prediction and Multipath Optimized Link State Routing Protokol.

This kinetic prediction and multipath extension of OLSR (MPOLSR) for NS2 is based on um-olsr and mpolsr.

Copyright (C) 2014 by Reza Andria Siregar (reza@ub.ac.id), I Komang Ari Mogi (arimogi@cs.unud.ac.id), and Waskitho Wibisono (waswib@if.its.ac.id).

This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY; without even the implied warranty of ERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
About how to install a new protocol in NS2, please refer to "Implementing a New Manet Unicast Routing Protocol in ns2", http://masimum.dif.um.es/?Documents. 

