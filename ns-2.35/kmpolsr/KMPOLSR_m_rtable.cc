/*              Copyright (C) 2010
	*             Multipath Extension by Jiazi Yi,                            *
 	*                   2007   Ecole Polytech of Nantes, France               * 
 	*                   jiazi.yi@univ-nantes.fr				   *
 	****************************************************************************
 	*    	This program is distributed in the hope that it will be useful,				*
	*    	but WITHOUT ANY WARRANTY; without even the implied warranty of				*
	*    	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 					*
 	**************************************************************************
*/

///
///\file KMPOLSR_m_rtable.cc
///\brief Implementation of our multipath routing table
///

//#include "kmpolsr/KMPOLSR.h"
#include "kmpolsr/KMPOLSR_m_rtable.h"
#include "kmpolsr/KMPOLSR_repositories.h"
using namespace std;
///
///\brief Creates a new empty routing table
///
KMPOLSR_m_rtable::KMPOLSR_m_rtable(){
	for (int i = 0;i<MAX_NODE; i ++)
	{
		//printf("m_rtable out_of_date[%i]\n", i);
		out_of_date[i] = true;
	}
}

///
///\brief Destroys the routing table and ll its entries
///
KMPOLSR_m_rtable::~KMPOLSR_m_rtable(){
	for(m_rtable_t::iterator it = m_rt_.begin();it!=m_rt_.end();it++)
		delete (*it).second;
		
}

m_rtable_t* KMPOLSR_m_rtable::m_rt(){
	return &m_rt_;
}
void KMPOLSR_m_rtable::set_flag(int id, bool flag){
	out_of_date[id] = flag;
}

void KMPOLSR_m_rtable::set_flag(bool flag){
	for (int i = 0; i < MAX_NODE; i ++){
		out_of_date[i] = flag;
	}
}

bool KMPOLSR_m_rtable::get_flag(int id){
	//printf("get_flag from %d \n", id);
	if (id > sizeof(out_of_date)) {
		return false;
	} else {
		return out_of_date[id];	
	}
}
///
///\brief Destroys the routing table and all its entries
///
void KMPOLSR_m_rtable::clear(){
	for (m_rtable_t::iterator it = m_rt_.begin();it != m_rt_.end(); it++)
		delete (*it).second;

	m_rt_.clear();
}


///
///\brief Deletes the entry whose destination is given.
///\param dest	address of the destiantion node.
///
void KMPOLSR_m_rtable::rm_entry(nsaddr_t dest){
/*	for (m_rtable_t::iterator it = m_rt_.begin(); it != m_rt_.end(); it++){
		if ( (KMPOLSR_m_rt_entry*)((*it).second).end() == dest)
			delete (*it).second;
	}
*/
	m_rt_.erase(dest);
	
}

///
///\brief finds the entries for the specified destination address
///\param dest destination address
///
m_rtable_t::iterator KMPOLSR_m_rtable::lookup(nsaddr_t dest){
	return m_rt_.find(dest);
}

///
///\brief add a new entry to the routing table
///\
///
void KMPOLSR_m_rtable::add_entry(KMPOLSR_m_rt_entry* entry,nsaddr_t addr){
/*
	KMPOLSR_m_rt_entry::iterator it = (*entry).end();
	nsaddr_t addr = (*it);
	m_rt_.insert(pair<nsaddr_t,KMPOLSR_m_rt_entry*>(addr,entry));*/
//	nsaddr_t addr = (*entry).addr
	m_rt_.insert(pair<nsaddr_t,KMPOLSR_m_rt_entry*>(addr,entry));
}
