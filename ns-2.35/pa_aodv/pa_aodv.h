/*
Copyright (c) 1997, 1998 Carnegie Mellon University.  All Rights
Reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The PA_AODV code developed by the CMU/MONARCH group was optimized and tuned by Samir Das and Mahesh Marina, University of Cincinnati. The work was partially done in Sun Microsystems.

*/

#ifndef __pa_aodv_h__
#define __pa_aodv_h__

//#include <agent.h>
//#include <packet.h>
//#include <sys/types.h>
//#include <cmu/list.h>
//#include <scheduler.h>

#include <cmu-trace.h>
#include <priqueue.h>
#include <pa_aodv/pa_aodv_rtable.h>
#include <pa_aodv/pa_aodv_rqueue.h>
#include <classifier/classifier-port.h>

/*
  Allows local repair of routes 
*/
#define PA_AODV_LOCAL_REPAIR

/*
  Allows PA_AODV to use link-layer (802.11) feedback in determining when
  links are up/down.
*/
#define PA_AODV_LINK_LAYER_DETECTION

/*
  Causes PA_AODV to apply a "smoothing" function to the link layer feedback
  that is generated by 802.11.  In essence, it requires that RT_MAX_ERROR
  errors occurs within a window of RT_MAX_ERROR_TIME before the link
  is considered bad.
*/
#define PA_AODV_USE_LL_METRIC

/*
  Only applies if PA_AODV_USE_LL_METRIC is defined.
  Causes PA_AODV to apply omniscient knowledge to the feedback received
  from 802.11.  This may be flawed, because it does not account for
  congestion.
*/
//#define PA_AODV_USE_GOD_FEEDBACK


class PA_AODV;

#define MY_ROUTE_TIMEOUT        10                      	// 100 seconds
#define ACTIVE_ROUTE_TIMEOUT    10				// 50 seconds
#define REV_ROUTE_LIFE          6				// 5  seconds
#define BCAST_ID_SAVE           6				// 3 seconds


// No. of times to do network-wide search before timing out for 
// MAX_RREQ_TIMEOUT sec. 
#define RREQ_RETRIES            3  
// timeout after doing network-wide search RREQ_RETRIES times
#define MAX_RREQ_TIMEOUT	10.0 //sec

/* Various constants used for the expanding ring search */
#define TTL_START     5
#define TTL_THRESHOLD 7
#define TTL_INCREMENT 2 

// This should be somewhat related to arp timeout
#define NODE_TRAVERSAL_TIME     0.03             // 30 ms
#define LOCAL_REPAIR_WAIT_TIME  0.15 //sec

// Should be set by the user using best guess (conservative) 
#define NETWORK_DIAMETER        30             // 30 hops

// Must be larger than the time difference between a node propagates a route 
// request and gets the route reply back.

//#define RREP_WAIT_TIME     (3 * NODE_TRAVERSAL_TIME * NETWORK_DIAMETER) // ms
//#define RREP_WAIT_TIME     (2 * REV_ROUTE_LIFE)  // seconds
#define RREP_WAIT_TIME         1.0  // sec

#define ID_NOT_FOUND    0x00
#define ID_FOUND        0x01
//#define INFINITY        0xff

// The followings are used for the forward() function. Controls pacing.
#define DELAY 1.0           // random delay
#define NO_DELAY -1.0       // no delay 

// think it should be 30 ms
#define ARP_DELAY 0.01      // fixed delay to keep arp happy


#define HELLO_INTERVAL          1               // 1000 ms
#define ALLOWED_HELLO_LOSS      3               // packets
#define BAD_LINK_LIFETIME       3               // 3000 ms
#define MaxHelloInterval        (1.25 * HELLO_INTERVAL)
#define MinHelloInterval        (0.75 * HELLO_INTERVAL)

/*
  Timers (Broadcast ID, Hello, Neighbor Cache, Route Cache)
*/
<<<<<<< HEAD
class PA_AODVBroadcastTimer : public Handler {
public:
        PA_AODVBroadcastTimer(PA_AODV* a) : agent(a) {}
=======
class PABroadcastTimer : public Handler {
public:
        PABroadcastTimer(PA_AODV* a) : agent(a) {}
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        void	handle(Event*);
private:
        PA_AODV    *agent;
	Event	intr;
};

<<<<<<< HEAD
class PA_AODVHelloTimer : public Handler {
public:
        PA_AODVHelloTimer(PA_AODV* a) : agent(a) {}
=======
class PAHelloTimer : public Handler {
public:
        PAHelloTimer(PA_AODV* a) : agent(a) {}
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        void	handle(Event*);
private:
        PA_AODV    *agent;
	Event	intr;
};

<<<<<<< HEAD
class PA_AODVNeighborTimer : public Handler {
public:
        PA_AODVNeighborTimer(PA_AODV* a) : agent(a) {}
=======
class PANeighborTimer : public Handler {
public:
        PANeighborTimer(PA_AODV* a) : agent(a) {}
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        void	handle(Event*);
private:
        PA_AODV    *agent;
	Event	intr;
};

<<<<<<< HEAD
class PA_AODVRouteCacheTimer : public Handler {
public:
        PA_AODVRouteCacheTimer(PA_AODV* a) : agent(a) {}
=======
class PARouteCacheTimer : public Handler {
public:
        PARouteCacheTimer(PA_AODV* a) : agent(a) {}
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        void	handle(Event*);
private:
        PA_AODV    *agent;
	Event	intr;
};

<<<<<<< HEAD
class PA_AODVLocalRepairTimer : public Handler {
public:
        PA_AODVLocalRepairTimer(PA_AODV* a) : agent(a) {}
=======
class PALocalRepairTimer : public Handler {
public:
        PALocalRepairTimer(PA_AODV* a) : agent(a) {}
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        void	handle(Event*);
private:
        PA_AODV    *agent;
	Event	intr;
};


/*
  Broadcast ID Cache
*/
class BroadcastID {
        friend class PA_AODV;
 public:
        BroadcastID(nsaddr_t i, u_int32_t b) { src = i; id = b;  }
 protected:
        LIST_ENTRY(BroadcastID) link;
        nsaddr_t        src;
        u_int32_t       id;
        double          expire;         // now + BCAST_ID_SAVE s
};

LIST_HEAD(pa_aodv_bcache, BroadcastID);


/*
  The Routing Agent
*/
class PA_AODV: public Agent {

  /*
   * make some friends first 
   */

        friend class pa_aodv_rt_entry;
<<<<<<< HEAD
        friend class PA_AODVBroadcastTimer;
        friend class PA_AODVHelloTimer;
        friend class PA_AODVNeighborTimer;
        friend class PA_AODVRouteCacheTimer;
        friend class PA_AODVLocalRepairTimer;
=======
        friend class PABroadcastTimer;
        friend class PAHelloTimer;
        friend class PANeighborTimer;
        friend class PARouteCacheTimer;
        friend class PALocalRepairTimer;
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c

 public:
        PA_AODV(nsaddr_t id);

        void		recv(Packet *p, Handler *);

 protected:
        int             command(int, const char *const *);
        int             initialized() { return 1 && target_; }

        /*
         * Route Table Management
         */
        void            rt_resolve(Packet *p);
        void            rt_update(pa_aodv_rt_entry *rt, u_int32_t seqnum,
		     	  	u_int16_t metric, nsaddr_t nexthop,
		      		double expire_time);
        void            rt_down(pa_aodv_rt_entry *rt);
        void            local_rt_repair(pa_aodv_rt_entry *rt, Packet *p);
 public:
        void            rt_ll_failed(Packet *p);
        void            handle_link_failure(nsaddr_t id);
 protected:
        void            rt_purge(void);

        void            enque(pa_aodv_rt_entry *rt, Packet *p);
        Packet*         deque(pa_aodv_rt_entry *rt);

        /*
         * Neighbor Management
         */
        void            nb_insert(nsaddr_t id);
        PA_AODV_Neighbor*       nb_lookup(nsaddr_t id);
        void            nb_delete(nsaddr_t id);
        void            nb_purge(void);

        /*
         * Broadcast ID Management
         */

        void            id_insert(nsaddr_t id, u_int32_t bid);
        bool	        id_lookup(nsaddr_t id, u_int32_t bid);
        void            id_purge(void);

        /*
         * Packet TX Routines
         */
        void            forward(pa_aodv_rt_entry *rt, Packet *p, double delay);
        void            sendHello(void);
        void            sendRequest(nsaddr_t dst);

        void            sendReply(nsaddr_t ipdst, u_int32_t hop_count,
                                  nsaddr_t rpdst, u_int32_t rpseq,
                                  u_int32_t lifetime, double timestamp);
        void            sendError(Packet *p, bool jitter = true);
                                          
        /*
         * Packet RX Routines
         */
        void            recvPA_AODV(Packet *p);
        void            recvHello(Packet *p);
        void            recvRequest(Packet *p);
        void            recvReply(Packet *p);
        void            recvError(Packet *p);

	/*
	 * History management
	 */
	
	double 		PerHopTime(pa_aodv_rt_entry *rt);


        nsaddr_t        index;                  // IP Address of this node
<<<<<<< HEAD
        u_int32_t       seqno;                  // Sequence Number //added by adi saputra, kayaknya ini untuk variabel hcjnya. berdasarkan routing zrp dan dokumentasinya.
=======
        u_int32_t       seqno;                  // Sequence Number
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
        int             bid;                    // Broadcast ID

        pa_aodv_rtable         rthead;                 // routing table
        pa_aodv_ncache         nbhead;                 // Neighbor Cache
        pa_aodv_bcache          bihead;                 // Broadcast ID Cache

        /*
         * Timers
         */
<<<<<<< HEAD
        PA_AODVBroadcastTimer  btimer;
        PA_AODVHelloTimer      htimer;
        PA_AODVNeighborTimer   ntimer;
        PA_AODVRouteCacheTimer rtimer;
        PA_AODVLocalRepairTimer lrtimer;
=======
        PABroadcastTimer  btimer;
        PAHelloTimer      htimer;
        PANeighborTimer   ntimer;
        PARouteCacheTimer rtimer;
        PALocalRepairTimer lrtimer;
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c

        /*
         * Routing Table
         */
        pa_aodv_rtable          rtable;
        /*
         *  A "drop-front" queue used by the routing layer to buffer
         *  packets to which it does not have a route.
         */
        pa_aodv_rqueue         rqueue;

        /*
         * A mechanism for logging the contents of the routing
         * table.
         */
        Trace           *logtarget;

        /*
         * A pointer to the network interface queue that sits
         * between the "classifier" and the "link layer".
         */
        PriQueue        *ifqueue;

        /*
         * Logging stuff
         */
        void            log_link_del(nsaddr_t dst);
        void            log_link_broke(Packet *p);
        void            log_link_kept(nsaddr_t dst);

	/* for passing packets up to agents */
	PortClassifier *dmux_;

<<<<<<< HEAD
        /*
        * Hop Count untuk Algoritma PA-SHORT
        */
        int             hci;
        int             hcj;

=======
>>>>>>> b864501f2f981bec71a80e2f7eeb857c9d95d67c
};

#endif /* __pa_aodv_h__ */
