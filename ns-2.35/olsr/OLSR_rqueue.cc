/*
Copyright (c) 1997, 1998 Carnegie Mellon University.  All Rights
Reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The OLSR code developed by the CMU/MONARCH group was optimized and tuned by Samir Das and Mahesh Marina, University of Cincinnati. The work was partially done in Sun Microsystems.
*/
//Modified by buda.hartawan@gmail.com


#include <assert.h>

#include <cmu-trace.h>
#include <olsr/OLSR_rqueue.h>
#include <time.h>       /* time */

#define CURRENT_TIME    Scheduler::instance().clock()
#define QDEBUG

/*
  Packet Queue used by OLSR.
*/

OLSR_rqueue::OLSR_rqueue() {
  head_ = tail_ = 0;
  len_ = 0;  //Panjang antrian
  limit_ = OLSR_RTQ_MAX_LEN; //Batas maksimum antrian
  timeout_ = OLSR_RTQ_TIMEOUT;
  len_ = rand() % 50 + 1;
}

void
OLSR_rqueue::enque(Packet *p) {
struct hdr_cmn *ch = HDR_CMN(p);

 /*
  * Purge any packets that have timed out.
  */
 purge();
 
 p->next_ = 0;
 ch->ts_ = CURRENT_TIME + timeout_;

 //printf ("\nLen = %d",len_);
 //printf ("\nLimit = %d",limit_);
 // JIka panjang antrian mencapai batas maksimum antrian
 if (len_ == limit_) {
 Packet *p0 = remove_head();	// decrements len_

   assert(p0); //Abort execution program while p0 
   //printf("HDR_CMN(p0)->ts_ = %f \n",HDR_CMN(p0)->ts_);
   //printf("CURRENT_TIME = %f \n",CURRENT_TIME);
   if(HDR_CMN(p0)->ts_ > CURRENT_TIME) {
     drop(p0, DROP_RTR_QFULL);
   }
   else {
     drop(p0, DROP_RTR_QTIMEOUT);
   }
 }
 //printf ("\nHead = %d",head_);
 //printf ("\nTail = %d",tail_);
 if(head_ == 0) {
   head_ = tail_ = p;
 }
 else {
   tail_->next_ = p;
   tail_ = p;
 }
 len_++;
#ifdef QDEBUG
   verifyQueue();
#endif // QDEBUG
}
                
/*
Packet*
OLSR_rqueue::deque() {
Packet *p;

 /*
  * Purge any packets that have timed out.
  */
 /*
 purge();

 p = remove_head();
#ifdef QDEBUG
 verifyQueue();
#endif // QDEBUG
 return p;

}*/


Packet*
OLSR_rqueue::deque(nsaddr_t dst) {
Packet *p, *prev;

 /*
  * Purge any packets that have timed out.
  */
 purge();

 findPacketWithDst(dst, p, prev);
 assert(p == 0 || (p == head_ && prev == 0) || (prev->next_ == p));

 if(p == 0) return 0;

 //Jika hanya terdapat head saja maka remove 
 if (p == head_) {
   p = remove_head(); //Fungsi remove_head me-return p
 }
 
 else if (p == tail_) {
   prev->next_ = 0;
   tail_ = prev;
   len_--;
 }
 else {
   prev->next_ = p->next_;
   len_--;
 }

#ifdef QDEBUG
 verifyQueue();
#endif // QDEBUG
 return p;

}

char 
OLSR_rqueue::find(nsaddr_t dst) {
Packet *p, *prev;  
	
 findPacketWithDst(dst, p, prev);
 if (0 == p)
   return 0;
 else
   return 1;

}

	
	

/*
  Private Routines
*/

Packet*
OLSR_rqueue::remove_head() {
Packet *p = head_;
        
 if(head_ == tail_) {
   head_ = tail_ = 0;
 }
 else {
   head_ = head_->next_;
 }

 if(p) len_--;

 return p;

}

void
OLSR_rqueue::findPacketWithDst(nsaddr_t dst, Packet*& p, Packet*& prev) {
  
  p = prev = 0;
  for(p = head_; p; p = p->next_) {
	  //		if(HDR_IP(p)->dst() == dst) {
	       if(HDR_IP(p)->daddr() == dst) {
      return;
    }
    prev = p;
  }
}


void
OLSR_rqueue::verifyQueue() {
Packet *p, *prev = 0;
int cnt = 0;

 for(p = head_; p; p = p->next_) {
   cnt++;
   prev = p;
 }
 assert(cnt == len_);
 assert(prev == tail_);

}


/*
void
OLSR_rqueue::purge() {
Packet *p;

 while((p = head_) && HDR_CMN(p)->ts_ < CURRENT_TIME) {
   // assert(p == remove_head());     
   p = remove_head();     
   drop(p, DROP_RTR_QTIMEOUT);
 }

}
*/

bool
OLSR_rqueue::findAgedPacket(Packet*& p, Packet*& prev) {
  
  p = prev = 0;
  for(p = head_; p; p = p->next_) {
    if(HDR_CMN(p)->ts_ < CURRENT_TIME) {
      return true;
    }
    prev = p;
  }
  return false;
}

void
OLSR_rqueue::purge() {
Packet *p, *prev;

 while ( findAgedPacket(p, prev) ) {
 	assert(p == 0 || (p == head_ && prev == 0) || (prev->next_ == p));

 	if(p == 0) return;

 	if (p == head_) {
   		p = remove_head();
 	}
 	else if (p == tail_) {
   		prev->next_ = 0;
   		tail_ = prev;
   		len_--;
 	}
 	else {
   		prev->next_ = p->next_;
   		len_--;
 	}
#ifdef QDEBUG
 	verifyQueue();
#endif // QDEBUG

	p = prev = 0;
 }

}

// Added by NVT
int
OLSR_rqueue::buffer(nsaddr_t current){
          
//printf("\n***************Node = %d's MAX buffer size %d ",current,limit_);
//printf("\nat Time %.4f Node = %d's MAX buffer size %d ",CURRENT_TIME,current,limit_);


return len_;

}
