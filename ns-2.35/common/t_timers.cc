/**
*  project: VoIP module for ns2
*  filename: t_timers.cc
*      author: C. Cicconetti <c.cicconetti@iet.unipi.it>
*               A. Erta <alessandro.erta@imtlucca.it>
*               A. Bacioccola <a.bacioccola@iet.unipi.it>
*       year: 2007
*  affiliation:
*       Dipartimento di Ingegneria dell'Informazione
*               University of Pisa, Italy
*  description:
*      Template timers (T_Timers)
*/

#include <t_timers.h>
 
