/**
   project: measure
   filename: tfunctor.cc
   author: A.Bacioccola <a.bacioccola@iet.unipi.it>
   year: 2007
   affiliation:
      Dipartimento di Ingegneria dell'Informazione
      University of Pisa, Italy
   description:
      class to manage function pointer
 */

#include <tfunctor.h>